FROM ubuntu:18.04

RUN apt-get -y update && apt install -y default-jre && apt install -y default-jdk && apt install -y build-essential && apt-get install -y wget

RUN ( wget https://downloads.apache.org/kafka/2.5.0/kafka_2.12-2.5.0.tgz ) \
	&& ( tar -xzf kafka_2.12-2.5.0.tgz ) \
	&& ( wget https://github.com/edenhill/librdkafka/archive/v1.5.0.tar.gz ) \
	&& ( tar -xzf v1.5.0.tar.gz )

CMD ( kafka_2.12-2.5.0/bin/zookeeper-server-start.sh kafka_2.12-2.5.0/config/zookeeper.properties > kafka_2.12-2.5.0/zookeeper_log.txt & ) \	
	&& ( sleep 20 ) \ 
	&& ( kafka_2.12-2.5.0/bin/kafka-server-start.sh kafka_2.12-2.5.0/config/server.properties > kafka_2.12-2.5.0/kakfa_log.txt & ) \
	&& ( sleep 30 ) \
	&& ( kafka_2.12-2.5.0/bin/kafka-topics.sh --create --bootstrap-server 127.0.0.1:9092 --replication-factor 1 --partitions 1 --topic test ) \
	&& ( cd librdkafka-1.5.0 ; ./configure ; make ) \
	&& ( nohup librdkafka-1.5.0/examples/rdkafka_complex_consumer_example_cpp -b localhost:9092 -g new_cons_gp test > librdkafka-1.5.0/L_log.txt 2>&1 & ) \
	&& ( sleep 60 ) \
	&& ( kafka_2.12-2.5.0/bin/kafka-topics.sh --delete --bootstrap-server localhost:9092 --topic test ) \
	&& ( tail -f librdkafka-1.5.0/L_log.txt )
